﻿using DesktopBackend;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesktopApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(ApplicationCommands.Close,
                new ExecutedRoutedEventHandler(delegate(object sender, ExecutedRoutedEventArgs args) { this.Close(); })));
        }

        private  void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var service = new WebApiClient();
            HttpResponseMessage message = await service.Login("Crowe", "111");

            if (message.IsSuccessStatusCode)
            {
                string user = await message.Content.ReadAsStringAsync();

                UserEntity userEntity = JsonConvert.DeserializeObject<UserEntity>(user);

                //Button button = (Button)sender;
                //button.Content = userEntity.username;

                MessageBox.Show(string.Format("Welcome {0}!", userEntity.username));
            }
        }
    }
}
